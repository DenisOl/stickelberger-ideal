# About

Computes the Stickelberger ideal for a multiquadratic field K = Q(\sqrt{d_1}, ..., \sqrt{d_n}), where all d_i = 1 mod 4, or one d_i = 2 mod 4. All d_i are pair-wise coprime and square free.

# Requirements:
- SageMath version 9.0.

# Running instructions:
Clone the repository files to some folder, then run SageMath and open file stickelberger.sage.

# Example:
To compute the Stickelberger ideal for the field Q(sqrt(-7), sqrt(13), run
```sage
load("stickelberger.sage")
gen_stickelberger([-7,13], 'output.txt')
```
Output will be the generators of the stickelberger ideal written as the rows of the matrix
```
[ 0  1  2  1]
[ 1  1  2  2]
[ 2  2  1  1]
[ 9 10  9  8]
[10  9  8  9]
[ 9  8  9 10]
[ 8  9 10  9]
```
This matrix will be written to output.txt. 