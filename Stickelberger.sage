# -*- coding: utf-8 -*-

d = [-7,13,-11,-19]  #Field
#dd = Combinations(d); #All subfields

#Number of CPUs
ncpus = 2

def check(d):
    
    #Checks field correctness. We allow only fields with d_i = 1 mod 4 and 2 mod 8.
    #Also all d_i must be coprime to each other.
    
    for i in range(len(d)):
        if Mod(d[i], 4) != 1 and Mod(d[i], 8) != 2:
            print('Incorrect input')
            d = 0
            subfields_c = 0
    if d != 0:
        a = create_labeles(d)
        b = create_labeles_inverse(d)
        subfields_c = []
        for i in range(len(b)):
            subfields_c.append(a[i])
        for i in range(len(d)):
            if Mod(d[i], 4) == 1:
                d[i] = abs(d[i])
            if Mod(d[i], 8) == 2:
                t = abs(d[i]/2)
                d[i] = []
                d[i].append(8)
                d[i].append(t)
    return d, subfields_c

def create_labeles(d):
    N = 2^(len(d)) #number of subfields
    labels = []
    for ctr in range(1, N):
        ctr_binary = (Integer(ctr).digits(2))
        subfield_label = [0]*len(d)
        for i in range(len(ctr_binary)):
            subfield_label[-i] = ctr_binary[i]
        labels.append(subfield_label)
    return sorted(labels)

def create_labeles_with_zerovector(d):
    N = 2^(len(d)) #number of subfields
    labels = []
    for ctr in range(0, N):
        ctr_binary = (Integer(ctr).digits(2))
        subfield_label = [0]*len(d)
        for i in range(len(ctr_binary)):
            subfield_label[-i] = ctr_binary[i]
        labels.append(subfield_label)
    return sorted(labels)

def create_labeles_inverse(d):
    s = create_labeles(d)
    ll = []
    for j in range(len(s)):
        l = []
        for i in range(len(d)):
            if s[j][i]*d[i] != 0:
                l.append(s[j][i]*d[i])
        ll.append(l)
    return ll

def xor(a, b):
    return eval("%s^%s" % (a, b))

@parallel(ncpus)
def ResPar(f, f1, f2, subfield, d, t_tmp):
    
    #Paralleled part of the function Restrictions.
    #Input: 
    #ncpus is the number of CPUs.
    #f = prod_{i=1}^{len(subfield)} d_i, where d_i \in subfield.
    #f1, f2 are segments of f. Example: if ncpus = 2, we will have 2 segments:
    #f1 = 0, f2 = floor(f/2) and f1 = floor(f/2), f2 = f.
    #subfield is the subfield of field d, whose restriction we want to compute.
    #d is the n-quadratic field, whose Stickelberger ideal we want to compute.
    #t_tmp is the list of fractions of the form f/d_i mod d_i.

    #Output:
    #res is the list of coefficients for the res \in Z[Gal] we are about to compute
    
    wt = sum(subfield)
    res = vector(QQ, 2**wt) # list of coefficients for the res \in Z[Gal] we are about to compute
    for a in range(f1, f2):
        if(gcd(a,f)!=1):
            continue;
                    
        sigma_tmp = 1
        ctr = 0 # counter to label the sigmas correctly
            
        a_index = []
        for j in range(len(subfield)):
                
            if subfield[j]==0:
                continue;

            if type(d[j]) is list:
                ind1 = mod(1/(t_tmp[ctr]*a), d[j][0])
                ind2 = mod(1/(t_tmp[ctr + 1]*a), d[j][1])
                    
                if (kronecker(8, ind1))*(kronecker(ind2, prod(d[j][k] for k in range(len(d[j])))/8)) == 1:
                    a_index.append(0)
                else:
                    a_index.append(1)
                ctr = ctr+2
            else:
                ind = mod(1/(t_tmp[ctr]*a), d[j])  # compute the index
                
                if kronecker(ind, d[j]) == 1:
                    a_index.append(0)
                else:
                    a_index.append(1)
                ctr = ctr+1
        aut = 0
        for i in range(len(a_index)):
            aut+=a_index[i]*2**((len(a_index) - 1) - i)
        res[aut] += a/f
    return(res)

def Restrictions(d, ncpus, subfields_c):
    
        #Input:
        #d is the n-quadratic field, whose Stickelberger ideal we want to compute.
        #ncpus is the number of CPUs in processor.
        #subfields_c is the list of subfields after "check" procedure.
    
        #the function outputs a list of restrictions each of the form
        #[subfield_label, [c_{sigma}] ], where rest = sum_{sigma \in Gal} c_{sigma} * sigma
        #the automorphisms sigma's appear wrt. to their  binary representation
        #(i.e., the coeff c_{id} is associated to all-0 and appears first)
        #and are sorted wrt. the binary repr. order
        
        #Example:
        #[1, 1, 1, 0], (71, 63, 64, 72, 63, 71, 72, 64)]
        #->
        #71*id1*id2*id3 + 63*id1*id2*sigma3 + 64*id1*sigma2*id3 + 72*id1*simga2*sigma3 +
        #63*sigma1*id2*id3 + 71*sigma1*id2*sigma3 + 72*sigma1*sigma2*id3 + 64*sigma1*sigma2*sigma3

    restrictions = []
    num = 0
    for subfield in subfields_c:
        #get the generators of this subfield
        ds = []
        for i in range(len(subfield)):
            if subfield[i]==1:
                if type(d[i]) is list:
                    for j in range(len(d[i])):
                        ds.append(d[i][j])
                else:
                    ds.append(d[i])
    
        f = prod(ds[i] for i in range(len(ds)))
            
        #precompute f/ds[i] mod ds[i]
        t_tmp = vector(ZZ, len(ds))
        for i in range(len(ds)):
            t_tmp[i] = mod(f/ds[i], ds[i])
        
        parlist = []
        if len(ds) < 4:
            ress = ResPar(f, 0, f, subfield, d, t_tmp)
            restrictions.append([subfield, ress])
            num += 1
        else:
            ress = 0
            for i in range(0, ncpus - 1):
                parlist.append((f, i*floor(f/ncpus), (i+1)*floor(f/ncpus), subfield, d, t_tmp))
            parlist.append((f, (ncpus-1)*floor(f/ncpus), f, subfield, d, t_tmp))
            
            res = sorted(list(ResPar(parlist)))
            ress = sum(res[i][1] for i in range(0, ncpus))
            
            restrictions.append([subfield, ress])
            num += 1
    
    return restrictions


def Corestrictions(d, rests):
    
        #Input:
        #d is the n-quadratic field, whose Stickelberger ideal we want to compute.
        #rests is a list of restrictions each of the form
        #[subfield_label, [c_{sigma}] ], where rest = sum_{sigma \in Gal} c_{sigma} * sigma.
    
        #returns coresctrictions, each of the form
        #[subfield_label, [c_i] ], where c_i is the coeff for the automorphism rho_i.
        #the auts rho_i's are sorted wrt. their binary representation
        
        #The function looks at the subfield label and lifts the auts of this subfield
        #to the auts of the big field by creating the correct labels (in binary)
        #of the lifited auts
        
        #lists zero_pos/one_pos determine the positions of 0's/1's in the subfield label
        
        #the correct index for rho (automorphisms of the big field) are formed by a combination
        #of binary strings from one-positions and zero-positions
        
        #A bit string aut_bin is formed by 1-positions and gives a ref. to the aut of the subfield
        #A bit string index_bin is formed by 0-position and for a *fixed* subfield automorphis varies
        #over all possible bit strings over the 0-positions
        
        #This way, for a fixed subfield automorphism we create 2^(zero_pos_n) different automorphisms
        #of the big field.
        
    
    corests = []
    
    for rest in rests:
        subfield = rest[0]
        restriction = rest[1]
    
    #print('subfield:', subfield)
        corest = vector(ZZ, 2**len(d))
        
        zero_pos = []
        one_pos = []
        for ind in range(len(subfield)):
            if subfield[ind] == 0: zero_pos.append(ind)
            else: one_pos.append(ind)
        
        one_pos_n = len(one_pos)
        zero_pos_n = len(zero_pos)

        for aut in range(len(restriction)): # run over the coos of this subfield's restriction
            aut_bin = Integer(aut).digits(2)
            aut_bin.reverse()
            aut_bin = [0]*(one_pos_n - len(aut_bin)) + aut_bin
            
            rho_index = vector(ZZ, len(d))
            ctr = 0
            for pos in one_pos:
                rho_index[pos] = aut_bin[ctr]
                ctr+=1
    
            for index in range(2**len(zero_pos)):

                index_bin = Integer(index).digits(2)
                index_bin.reverse()
                index_bin = [0]*(zero_pos_n - len(index_bin)) + index_bin

                ctr = 0
                for pos in zero_pos:
                    rho_index[pos] = index_bin[ctr]
                    ctr+=1
                
                rho_index_L = list(rho_index)
                rho_index_L.reverse()
                rho_index_int = sum([rho_index_L[i]*2**i for i in range(len(rho_index_L))])
                #print(rho_index, rho_index_int)
                    
                corest[rho_index_int] = restriction[aut]


        corests.append([subfield, corest])
            
    return corests

def FinalMatrix(d, corests):
    
        #Input:
        #d is the n-quadratic field, whose Stickelberger ideal we want to compute.
        #corests is a list of coresctrictions, each of the form
        #[subfield_label, [c_i] ], where c_i is the coeff for the automorphism rho_i.

        #Output:
        #Matrix A of all Stickelberger ideal generators.

        #The function computes all Stickelberger ideal generators. It multiplies every
        #rho (automorphisms of the big field) by every corestriction. All non-repeatable results
        #we collect in list comb. After that, we create a matrix A, where every string is the
        #one combination from comb.
    
    cor = []
    for i in range(len(corests)):
        cor.append(corests[i][1])

    comb = []
    full = create_labeles_with_zerovector(d)

    for s in range(len(cor)):
        for k in range(len(full)):
            resfull = []
            for i in range(len(full)):
                resfull.append([])
                for j in range(len(full[k])):
                    resfull[i].append(xor(full[k][j], full[i][j]))

            comb_t = vector(ZZ, len(cor[s]))
            for ii in range(len(full)):
                for jj in range(len(resfull)):
                    if resfull[jj] == full[ii]:
                        comb_t[jj] = cor[s][ii]
                        ii == len(full) # break the outer loop
                        break  # break the inner loop
            if comb_t not in comb:
                comb.append(comb_t)

    A = matrix(ZZ, len(comb), 2**len(d))
    for i in range(len(comb)):
        A[i] = comb[i]
    
    return A


def find_non_zero_min(a):
		# finds minimal element in vector a
    min_a = oo
    for i in range(len(a)):
        if a[i]<min_a and a[i]!=0:
            min_a = a[i]

    return min_a

def remove_min_wisely(vec, min):
		# subtract from vector vec the vector consisting of (min_i vec[i])
		# Reason: for any ideal I, the product prod_sigma (sigma(I)) is principal
		#(and equal to the norm of I), where product is taken
		# over all elements from the Galois group
		# 
    new_vec = vector([0]*len(vec))
    for i in range(len(vec)):
        if vec[i]!=0:
            new_vec[i] = vec[i] - min
    return new_vec


def gen_stickelberger(d, filename, verbose = False):
		#Computes stickelberger ideal in the form of matrix.
		#Input: d is the n-quadratic field, whose Stickelberger ideal we want to compute.
		#filename - the file we want to write the result.
		#Output: Matrix A of all Stickelberger ideal generators.
		#
    c = check(d)
    d = c[0]
    subfields_c = c[1]

    rest = Restrictions(d, ncpus, subfields_c)


    corest = Corestrictions(d, rest)
		#ff.write('Corestrictions:\n')
		#ff.write(str(corest) + "\n")

    A = FinalMatrix(d, corest)
    i = 0
    for arow in A:
        minr = find_non_zero_min(arow)
        row_tmp = remove_min_wisely(arow, minr)
        A.set_row(i, row_tmp)
    if verbose:
        ff = open(filename,'w')
        ff.write('Stickelberber for the field :', d, '\n')
        for i in range(A.nrows()):
            ff.write(str(A[i]) + "\n")
        ff.close()
    return A


